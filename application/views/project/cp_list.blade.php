@layout('templates.admin')
@section('content')
        <div class="right">
            <a class="btn" href="{{URL::to('admin/projects/new_project')}}">Novo Projecto</a>
            <a class="btn" href="{{URL::to('admin/projects/export')}}">Exportar</a>
        </div>
        <table class="table table-striped table-bordered">
                <thead>
                        <tr>
                                <th>Title</th>
                                <th>Publicado</th>
                                <th>Operações</th>
                        </tr>

                </thead>
                <tbody>
                @foreach($projects as $project)

                <tr>
                    <td>{{$project->title}}</td>
                    <td>{{$project->created_at}}</td>
                     <td><a rel="tooltip" title="Editar" href="{{URL::to_route('edit_project',array($project->id))}}"><i class="icon-pencil"></i></a><a rel="tooltip" title="Apagar" href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                    <a rel="tooltip" title="Ver Artigo" href="{{URL::to_route('view_project',array($post->slug))}}"><i class="icon-eye-open"></i></a></td>
                </tr>
          @endforeach
                </tbody>
        </table>


                    <div class="modal hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">AVISO!</h3>
                      </div>
                      <p>Deseja desactivar este projecto {{$project->title}}?</p>
                      {{Form::open('admin/projects/delete','DELETE')}}
                      {{Form::hidden('id',$project->id)}}
                      <div class="modal-footer">
                        
                        <button class="btn" aria-hidden="true" id="Delete">Sim</button>
                        {{Form::close()}}
                        <button class="btn" data-dismiss="modal" aria-hidden="true" id="cancel">Não</button>
                      </div>
                    </div>
@endsection
