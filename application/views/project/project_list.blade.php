@layout('templates.main')

@section('container')

      
      <!-- content right -->
      <div id="content_left">
        
       @foreach($projects as $project)

        <div class="blog-post">
          
          <ul class="blog-post-info">
            <li>{{HTML::image('images/blog/date.png')}} {{$project->created_at}}</li>
          </ul>
          
          <div class="blog-post-text ">
            <h3><a href="{{URL::to_route('view_project',array($project->slug))}}">{{$project->title}}</a></h3>
            
            <p>{{Str::words($project->project_body,40)}}</p>
           <a href="{{URL::to_route('view_project',array($project->slug))}}"><span class="label">Mais...</span></a>
          </div>
        </div>
        
          <div class="clear"></div>
          <div class="divider"></div>
        
        @endforeach
        
        <!-- portfolio nav -->
        <div id="portfolio-nav">
        </div>
        
      </div>
        
      <div class="clear"></div>
 

  <!-- END content -->
  


@endsection
