@layout('templates.main')
@section('container')


    <div id="content_left">
        <div class="blog-post">
          <ul class="blog-post-info">
            @if($project->cover)
            {{HTML::image('img/logotipo.png', 'Sem Cover!')}}
          @else
            <img src="{{$project->cover}}">
          @endif
          <center><h1><a href="{{URL::to_route('view_project',array($project->slug))}}">{{$project->title}}</a></h1></center>
      
            <li>{{HTML::image('images/blog/date.png')}} {{$project->created_at}}</li>
          </ul>
          <div class="blog-post-text">
           <p>  {{$project->project_body}}</p>
          </div>
        </div>
        <div class="clear"></div>
          <div class="divider"></div>
      
    </div>
          

@endsection