@layout('templates.main')

@section('container')

      <!-- content right -->
      <div id="content_left">

       @foreach($posts->results as $post)

        <div class="blog-post">
          <center>
          @if($post->cover)

          {{HTML::image($post->cover,'cover',array('class'=>'image-holder img'))}}
          @endif
        </center>
          <br />
          <ul class="blog-post-info">
            <li>{{HTML::image('images/blog/cat.png')}}in
            <?php $tags = $post->tags()->order_by('tag_name')->get(); ?>
            @foreach($tags as $tag)
            <a href="{{URL::to_route('view_cat',array($tag->tag_name))}}"><span class="label label-info">{{$tag->tag_name}}</span></a>
            @endforeach</li>
            <li>{{HTML::image('images/blog/date.png')}} {{$post->created_at}}</li>
          </ul>

          <div class="blog-post-text ">
            <h3><a href="{{URL::to_route('view_article',array($post->slug))}}">{{$post->title}}</a></h3>

            <p>{{Str::words($post->post_body,40)}}</p>
           <a href="{{URL::to_route('view_article',array($post->slug))}}"><span class="label">Mais...</span></a>
          </div>
        </div>

          <div class="clear"></div>
          <div class="divider"></div>

        @endforeach
        <center>
        <!-- portfolio nav -->
        <div id="portfolio-nav">
         {{$posts->links()}}
        </div>
       </center>
      </div>

      <div class="clear"></div>



  <!-- END content -->



@endsection
