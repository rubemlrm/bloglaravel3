@layout('templates.main')

@section('container')
  <div class="row-fluid">
            <div class="hero-unit span12">
                 @if($settings->subtitle != "" && $settings->intro != "")
                  <h3><i class="icon-group"></i>   {{$settings->subtitle}}</h3>
                  {{$settings->intro}}
                 @endif
            
           </div>
      </div>
 @endsection

