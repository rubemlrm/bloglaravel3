@layout('templates.main')
@section('container')



        <div class="sixteen columns">
      
          <div class="eleven columns alpha content">

            <article class="post type-post format-standard clearfix">
              <div class="featured-image">
                <a href="#">
                  <img src="img/temp/blog_1.jpg" alt="">
                </a>
              </div>
              <div class="three columns alpha meta">
                <ul>
                  <li class="clearfix type"><a href="#" class="article">Article</a></li>
                  <li><strong>Posted:</strong> Aug 28 2012</li>
                  <li><strong>Type:</strong> <a href="#">Article</a></li>
                  <li><strong>Comments:</strong> <a href="#">5</a></li>
                </ul>
              </div>
              <div class="eight columns omega text">
                <h2><a href="#">Lorem ipsum dolor sit amet.</a></h2>
                gerado por php

              </div>
            </article> 

            <div class="comments">
              <h3>5 Comments</h3>

              <ol>
                <li class="comment even clearfix">
                  <div class="avatar columns alpha">
                    <a href="#">
                      <img src="img/user.png" alt="">
                    </a>
                  </div>  
                  <div class="nine columns alpha text">
                    <a href="#">
                      <h5><a href="#">Filip Stefansson</a> <span>- Aug 28, 2012 at 20:24</span></h5>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                    </a>
                  </div>  
                </li>

                
              </ol>

              <h3>Envie um comentário</h3>

              <?php echo Form::open('view');
              	
              	echo Form::label('nome','Nome');
              	echo Form::text('nome');

              	echo Form::label('email','Email');
              	echo Form::email('email');

              	echo Form::label('site','Website');
              	echo Form::text('site');

              	echo Form::label('mensagem','Mensagem');
              	echo Form::text('mensagem');
              	?><p><?
              	echo Form::submit('Envie o seu comentário', array('class'=>'button branded'));
              	echo Form::close();?></p>
              </div>
         
          </div><!-- /content -->

          <aside class="five columns omega sidebar">
            
            <div class="widget search">
              <form>
                <input type="input" placeholder="Enter keyword and press enter…" name="s" id="search" results="5">
              </form>
            </div><!-- /search -->

            <div class="widget clearfix categories">
              <h3>Categories</h3>
              <ul>
                <li><a href="#">Web dev</a></li>
                <li><a href="#">Responsive</a></li>
                <li><a href="#">Design</a></li>
                <li><a href="#">Themes</a></li>
              </ul>
            </div>

          </aside>

        </div>

@endsection