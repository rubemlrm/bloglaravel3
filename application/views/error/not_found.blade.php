@layout('templates.main')
@section('container')

  <div class="container">


        <div class="row">
          
          <div class="span9">
            <div class="post">
            <div class="row">
          <div class="span2">
         
       
        </p>
          </div><!-- end featured image -->

          <div class="details span7">
           
             <h4><span class="alert alert-error">{{$error}}</span></h4>
          
            </div> <!-- end details -->
          </div><!-- end post row -->
            <div class="row">
    </div>
        </div><!-- end post -->
        <hr>
          <div class="span9">
          
        </div>
          </div>

          <aside class="span3">

            <div class="widget span3">
              <h3>Categorias</h3>
              <ul class="side-bar nav nav-tabs nav-stacked">
                 @foreach($taglist as $tag)


                  <li><a href="{{URL::to_route('view_cat',array($tag->tag_name))}}">{{$tag->tag_name}}</a></li>
                @endforeach
              </ul>
            </div>

            </div><!-- end widget row -->

          </aside> <!-- end sidebar -->

        </div><!-- end row -->
      </div>
    </div>

@endsection