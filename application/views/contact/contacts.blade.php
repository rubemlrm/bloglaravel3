@layout('templates.main')

@section('container')
    <div class="container">
      <div class="row">
      <div class="hero-unit">
          <h1>Contactos!</h1>
          <p class="tagline">Desde já os meu profundos agradecimentos por ter visitado a minha página pessoal. Caso tenha alguma questão não exite em contactar-me!</p>  
        </div><!--end hero unit -->
     <section class="span6">
            @if(Session::has('success'))
             <span>{{Session::get('success')}}</span>
            @endif
           {{ Form::open('contacts', 'POST', array('enctype'=>'multipart/form-data')) }}
              <table>
                <tr>
             <td> <label class="control-label" for="inputName"><i class="icon-user"></i> Nome Completo</label>
              <div class="controls controls-row">
                  {{ Form::text('contact_name', Input::old('contact_name'),array('id'=>'message','placeholder'=>'introduza o seu nome')) }}
                  {{$errors->first('name','<span class="alert alert-error">:message</span>')}}
                
              </div>
            </td></tr>
            <tr><td>
              <label class="control-label" for="inputEmail"><i class="icon-envelope"></i> Email</label>
              <div class="controls">
                  {{ Form::text('contact_email', Input::old('contact_email'),array('id'=>'message','placeholder'=>'introduza o seu email')) }}
               {{$errors->first('email','<span class="alert alert-error">:message</span>')}}
              </div>
            </td></tr>
            <tr><td>
              <label class="control-label" for="inputSubject"><i class="icon-question-sign"></i> Assunto</label>
              <div class="controls">
                {{ Form::text('contact_subject', Input::old('contact_subject'),array('id'=>'message','placeholder'=>'introduza o assunto')) }}
                {{$errors->first('subject','<span class="alert alert-error">:message</span>')}}
              </div>
            </td></tr>
            <tr><td>
              <label class="control-label" for="inputMessage"><i class="icon-pencil"></i> Mensagem</label>
              <div class="controls">
                {{ Form::textarea('contact_message', Input::old('contact_message'),array('id'=>'message','placeholder'=>'Descreva o que pretende!')) }}
                {{$errors->first('message','<span class="alert alert-error">:message</span>')}}
              </div>
            </td></tr>
            <tr><td>
                <div class="controls">
                  <button type="submit" class="button-contact">Enviar mensagem</button>
                </div>
              </td></tr>
            </table>
            {{ Form::token() . Form::close() }}


                
            </section><!-- end left -->

            <section class="span6">
           
            </section><!--end right --> 
          </div><!--end row -->
        </div>
      </div>
@endsection
