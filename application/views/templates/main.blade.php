<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
    <![endif]-->
    <!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8">
        <![endif]-->
        <!--[if IE 8]>
        <html class="no-js lt-ie9">
            <![endif]-->
            <!--[if gt IE 8]>
            <!-->
            <html class="no-js">
                <!--<![endif]-->
<head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <title></title>

                <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
                {{HTML::style('css/style.css')}}
        {{HTML::style('css/buttons.css')}}
        {{HTML::style('css/responsive.css')}}
        {{HTML::style('css/layout.css')}}
        {{HTML::style('http://fonts.googleapis.com/css?family=Ubuntu:400,500,400italic,500italic')}}
        {{HTML::style('http://fonts.googleapis.com/css?family=Lato:300,400,700')}}
        {{HTML::style('js/tiny_mce/plugins/syntaxhighlighter/styles/core/shCoreEclipse.css')}}
        {{HTML::style('js/tiny_mce/plugins/syntaxhighlighter/styles/core/shCore.css')}}
</head>
<body>
                <!--[if lt IE 7]>
                <p class="chromeframe">
                    You are using an outdated browser.
                    <a href="http://browsehappy.com/">Upgrade your browser today</a>
                    or
                    <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a>
                    to better experience this site.
                </p>
                <![endif]-->

                <!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->

                <div id="top">
                    <div class="wrap">

                        <!-- logo -->
                        <div id="logo">
                            <a href="#">
                                <h1>Rubemlrm.com</a>
                                <!-- logo --> </div>
                            <!-- nav -->
                            <div id="nav">
                                <ul id="nav2">
                                    <li>
                                        <a href="{{URL::to('/')}}">Home Page</a>
                                    </li>
                                    <li>
                                        <a href="{{URL::to('projects')}}">Projectos</a>
                                    </li>
                                    <li>
                                        <a href="{{URL::to('contacts')}}">Contactos</a>
                                    </li>
                                </ul>
                                <div class="clear"></div>
                            </div>

                            <div class="clear"></div>

                        </div>
                    </div>
                    <!-- END top -->

                    <div class="container">
                        <div id="content_wp">
                            <div class="wrap">
                                <div id="sidebar_right">
                                    <div class="sidebar_area">
                                        <h3>Categorias</h3>
                                        <ul class="sidebar_area_nav">
                                            @foreach($taglist as $tag)
                                            <li>
                                                <a href="{{URL::to_route('view_cat',array($tag->tag_name))}}">{{$tag->tag_name}}</a>
                                                <br />
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="space2"></div>
                                </div>
                                @yield('container')
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <br/>
                    <br />
                    <!-- START footer -->
                    <div id="footer">
                        <div class="wrap">

                            <div class="one_third">
                                <!-- text widget -->
                                <div class="footer-heading">
                                    <h3>Projectos:</h3>
                                    @foreach($projects as $project)
                                    <?php $date = preg_split('/ /',$project->
                                    created_at);?>
                                    <?php $pub_date=preg_split('/-/',$date[0]);?>

                                    <li> <b>{{$pub_date[2]."/".$pub_date[1]}}:</b>
                                        <a href="{{URL::to_route('view_project',array($project->slug))}}">{{$project->title}}</a>
                                    </li>
                                    @endforeach
                                </div>
                            </div>

                            <div class="one_third">
                                <!-- latest tweets -->
                                <div class="footer-heading">
                                    <h3>Artigos:</h3>
                                    @foreach($articles as $article)
                                    <?php $date = preg_split('/ /',$article->
                                    created_at);?>
                                    <?php $pub_date=preg_split('/-/',$date[0]);?>

                                    <li> <b>{{$pub_date[2]."/".$pub_date[1]}}:</b>
                                        <a href="{{URL::to_route('view_article',array($article->slug))}}">{{$article->title}}</a>
                                    </li>
                                    @endforeach
                                </div>

                            </div>

                            <div class="one_third last">
                                <!-- get in touch details -->
                                <div class="footer-heading">
                                    <h3>Tweets:</h3>
                                </div>
                                <div class="tweet"></div>

                            </div>

                            <div class="clear"></div>

                        </div>
                    </div>

                    <div id="copyright">
                        <div class="wrap">
                            <center>
                                <p>
                                    Copyright &copy; 2012
                                    <a href="http://www.rubemlrm.com/">Rubemlrm</a>
                                    . All Rights Reserved.
                                    <span id="back-top">
                                        &nbsp;
                                        <a href="#body">Top &uarr;</a>
                                    </span>
                                </p>
                            </center>

                        </div>
                    </div>
                    {{HTML::script('scripts/jquery.min.js')}}
        {{HTML::script('js/shCore.js')}}
        {{HTML::script('scripts/tinynav.js')}}
        {{HTML::script('scripts/main.js')}}
        {{HTML::script('scripts/options.js')}}
        {{HTML::script('scripts/hoverIntent.html')}}
        {{HTML::script('scripts/superfish.js')}}
        {{HTML::script('scripts/jquery.tweet.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/core/shCore.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushPerl.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushPhp.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushBash.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushCss.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushCpp.js')}}
        {{HTML::script('js/tiny_mce/plugins/syntaxhighlighter/scripts/brushes/shBrushPython.js')}}
                    <script type="text/javascript">SyntaxHighlighter.all()</script>
                    <script type="text/javascript">
        $(function () {
            $("#nav ul#nav2").tinyNav();
        });
    </script>

                    <script type="text/javascript" src="scripts/slides.min.jquery.js"></script>
                    <script type="text/javascript">
        $(function(){
            $('#slides').slides({
                preload: true,
                generateNextPrev: true,
            });
            $('#slides2').slides({
                preload: true,
                generateNextPrev: true,
            });
        });
    </script>
</body>
                </html>