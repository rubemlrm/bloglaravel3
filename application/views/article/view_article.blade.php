@layout('templates.main')
@section('container')

    
    <div id="content_left">
        <div class="blog-post">

          <center>
             @if($post->cover)
          {{HTML::image('uploads/thumbnails/articles/'.$post->cover,'cover',array('class'=>'image-holder img'))}}
          @endif
        </center>
          
           <center><h1><a href="{{URL::to_route('view_article',array($post->slug))}}">{{$post->title}}</a></h1></center>
      
           <p>  {{$post->post_body}}</p>
          </div>
        </div>
        <div class="clear"></div>
          <div class="divider"></div>
      


@endsection