<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Coming Soon Page</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Stylesheets -->
	{{HTML::style('offline/css/reset.css')}}
	{{HTML::style('offline/css/styles.css')}}

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- JS -->
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="js/countdown.js"></script>
	
	<script>
	
		$(document).ready(function(){
			$("#countdown").countdown({
				date: "17 december 2012 12:00:00",
				format: "on"
			},
			
			function() {
				// callback function
			});
		});
	
	</script>
</head>
<body>

	<!-- LOGO -->
	<header class="container">
		<a href="index.html" id="logo"><img src="images/rubemlrm.png" alt="Coming Soon Page" /></a>
	</header>
	
	
	<!-- TIMER -->
	<div class="timer-area">
		
		<h1>Novidades brevemente!</h1>
		
		<ul id="countdown">
			<li>
				<span class="days">00</span>
				<p class="timeRefDays">days</p>
			</li>
			<li>
				<span class="hours">00</span>
				<p class="timeRefHours">hours</p>
			</li>
			<li>
				<span class="minutes">00</span>
				<p class="timeRefMinutes">minutes</p>
			</li>
			<li>
				<span class="seconds">00</span>
				<p class="timeRefSeconds">seconds</p>
			</li>
		</ul>
		
	</div> <!-- end timer-area -->
	

	
	
	
	<!-- FOOTER -->
	<footer id="main-footer">
		<p>&copy; Copyright 2012 Rubemlrm.com All rights reserved.</p>
	
	</footer>

</body>
</html>