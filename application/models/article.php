<?php 

class Article extends Eloquent
{
    public static $timestamps = true;
    public function author()
    {
        return $this->belongs_to('User', 'author_id');
    }
    public function tags(){
		return $this->has_many_and_belongs_to('Tag');
	}
}
