<?php

class Article_Controller extends Controller{

	public $restful = true;

	public function get_view_article($id){
		$articles = Article::order_by('created_at','desc')->take(5)->get();
		$projects = Project::order_by('created_at','desc')->take(5)->get();
	    $tags = Tag::all();
		$post = Article::where('slug','=',$id)->first();
		$error = 'ERRO: Artigo não existe!';
		if(!($post)){
			return View::make('error.not_found')
				->with('error',$error)
				->with('articles',$articles)
		        ->with('taglist',$tags)
		        ->with('projects',$projects);

		}else{
		return View::make('article.view_article')
				->with('post', $post)
				->with('articles',$articles)
		        ->with('taglist',$tags)
		        ->with('projects',$projects);
		}
	}

	public function get_view_cat($id){    
		$tags = Tag::all();
   		$articles = Article::order_by('created_at','desc')->take(5)->get();
   		$projects = Project::order_by('created_at','desc')->take(5)->get();
		$post = Tag::with('articles')->where('tag_name', '=', $id)->first();
		$error = 'ERRO: Categoria não existe';
		if(!($post)){
		return  View::make('error.not_found')
			->with('error',$error)
			->with('post', $post)
			->with('articles',$articles)
		    ->with('taglist',$tags)
		   	->with('projects',$projects); 
		}else{
		return View::make('article.view_cat')
				->with('post', $post)
				->with('articles',$articles)
		        ->with('taglist',$tags)
		        ->with('projects',$projects);	
		}
	}

	public function get_articles(){
	   $articles = Article::order_by('created_at','desc')->take(5)->get();
	   $posts = Article::with('author')->order_by('created_at','desc')->paginate(5);
	   $projects = Project::order_by('created_at','desc')->take(5)->get();
	   $tags = Tag::all();
	   $error = "Não existem artigos!\n";
	   if($articles){
	    return View::make('pages/blog')
	        ->with('posts', $posts)
	        ->with('articles',$articles)
	        ->with('taglist',$tags)
	        ->with('projects',$projects);
	   }else{
	   	return View::make('pages/blog')
	   		->with('error',$error)
	        ->with('posts', $posts)
	        ->with('articles',$articles)
	        ->with('taglist',$tags)
	        ->with('projects',$projects);
	   }
	}

}
