<?php 

class Contact_Controller extends Controller{
	public $restful = true;

	public function get_new_contact(){
        $articles = Article::order_by('created_at','desc')->take(5)->get();
        $projects = Project::order_by('created_at','desc')->take(5)->get();
		$tags = Tag::all();
		return View::make('contact.contacts')
            ->with('articles', $articles)
            ->with('projects', $projects)
            ->with('taglist',$tags);

	}

	public function post_new_contact(){
		$success ="Pedido de contacto enviado com sucesso!";
		$new_post =  array(
        'name'    => Input::get('contact_name'),
        'email'     => Input::get('contact_email'),
        'subject'   => Input::get('contact_subject'),
        'message' => Input::get('contact_message'),
        );
    	
	    $rules = array(
            'name'     => 'required|min:3|max:255',
            'email'  => 'required|email',
            'subject' => 'required|min:5|max:50',
            'message' => 'required|min:5'
	    );
	     
	    $validation = Validator::make($new_post, $rules);
	    if ( $validation -> fails() )
	    {
	         
	        return Redirect::to('contacts')
	                ->with_errors($validation)
 	                ->with_input();
        }else{
        	$new_contact = new Contact($new_post);
		    $new_contact->save();
		    return Redirect::to('contacts')
		    	->with('success',$success);	    	
        }   

        

	}

	public function get_contact_list(){
		return View::make('contact.cp_list');
	}

}