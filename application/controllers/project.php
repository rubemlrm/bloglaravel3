<?php

class Project_Controller extends Controller{
	public $restful = true;
	
	public function get_projects(){
		 $articles = Article::order_by('created_at','desc')->take(5)->get();
  		 $projects_list = Project::all();
  		 $projects = Project::order_by('created_at','desc')->take(5)->get();
  		 $tags = Tag::all();
         $error = "ERRO: Não existem projectos!\n";
         if($projects){
         return View::make('project.project_list')
           ->with('projects', $projects)
           ->with('articles',$articles)
           ->with('taglist',$tags)
           ->with('projects_list',$projects_list);
         }else{
         	return View::make('error.not_found')
           ->with('projects', $projects)
           ->with('articles',$articles)
           ->with('taglist',$tags)
           ->with('error',$error)
                      ->with('projects_list',$projects_list);

         }
	}

	public function get_list(){
		$projects = Project::all();
		return View::make('project.cp_list')
			->with('projects',$projects);
	}

	public function get_view_project($id){
		$articles = Article::order_by('created_at','desc')->take(5)->get();
		$projects = Project::order_by('created_at','desc')->take(5)->get();
	    $tags = Tag::all();
		$project = Project::where('slug','=',$id)->first();
		$error = 'ERRO: Projecto não existe';
		if(!($project)){
			return View::make('error.not_found')
				->with('error',$error)
				->with('articles',$articles)
		        ->with('taglist',$tags)
		        ->with('projects',$projects);
		}else{
		return View::make('project.view_project')
				->with('project', $project)
				->with('articles',$articles)
		        ->with('taglist',$tags)
		        ->with('projects',$projects);
		}
	}
}